﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Npgsql;
using Otus.Teaching.Concurrency.Import.Core.StorageManagers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;


namespace Otus.Teaching.Concurrency.Import.WebAPI.Controllers
{
    [ApiController]
    [Route("customers")]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerStorageManager _customers;

        public CustomerController(ICustomerStorageManager customers)
        {
            _customers = customers;
        }

        [HttpGet]
        [Route("get")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Customer))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Get(int id)
        {
            var result = _customers.GetCustomer(id);
            if (result == null) return NotFound();
            return Ok(result);
        }

        [HttpPost]
        [Route("create")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(int))]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public IActionResult CreateCustomer([FromBody] Customer entity)
        {
            try
            {
                var rowsAffected = _customers.CreateCustomer(entity);
                return Ok(rowsAffected);
            }
            catch (NpgsqlException e)
            {
                return Conflict(e);
            }
        }
    }
}
