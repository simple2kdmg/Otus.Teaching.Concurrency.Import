﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Serializers;
using Otus.Teaching.Concurrency.Import.DataAccess.StorageManagers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class ParallelDataLoader : IDataLoader
    {
        private readonly ISerializer<Customer> _serializer;
        private readonly string _connectionString;
        private readonly int _maxThreadCount;

        public ParallelDataLoader(ISerializer<Customer> serializer, string connectionString, int maxThreadCount = 64)
        {
            if (maxThreadCount < 1 || maxThreadCount > 64)
                throw new ArgumentOutOfRangeException(nameof(maxThreadCount), "Value should be in range [1, 100].");
            _serializer = serializer;
            _connectionString = connectionString;
            _maxThreadCount = maxThreadCount;
            Console.WriteLine($"Data loader initialized. MaxThreadCount = {maxThreadCount}.{Environment.NewLine}");
        }

        public void LoadData(string dataFilePath)
        {
            var customersData = File.ReadAllText(dataFilePath);
            var customers = _serializer.DeserializeList(customersData);
            var jobGroups = GetJobGroups(customers);
            RunJobs(jobGroups);
        }

        private List<List<Customer>> GetJobGroups(List<Customer> customers)
        {
            var groupCapacity = customers.Count / _maxThreadCount;
            var step = groupCapacity > 100 ? groupCapacity : 100;
            var orderedCustomers = customers.OrderBy(x => x.Id).ToList();
            var jobGroups = new List<List<Customer>>{new List<Customer>()};
            int? referenceId = null;
            int groupIndex = 0;

            foreach (var customer in orderedCustomers)
            {
                referenceId ??= customer.Id;
                if (customer.Id - referenceId < step || groupIndex == _maxThreadCount - 1) jobGroups[groupIndex].Add(customer);
                else
                {
                    referenceId = customer.Id;
                    groupIndex++;
                    jobGroups.Add(new List<Customer> {customer});
                }
            }

            return jobGroups;
        }

        private void RunJobs(List<List<Customer>> jobGroups)
        {
            var sw = new Stopwatch();
            sw.Start();
            var handlers = new WaitHandle[jobGroups.Count];
            for (int i = 0; i < jobGroups.Count; i++)
            {
                var index = i;
                var handler = new AutoResetEvent(false);
                handlers[index] = handler;
                ThreadPool.QueueUserWorkItem(UpdateCustomersTable, new StateWrapper
                {
                    WaitHandle = handler,
                    JobIndex = index,
                    Customers = jobGroups[index]
                });
            }
            WaitHandle.WaitAll(handlers);
            sw.Stop();
            Console.WriteLine($"Jobs completed. Duration: { (decimal)sw.ElapsedMilliseconds / 1000 } s.");
        }

        private void UpdateCustomersTable(object state)
        {
            var stateWrapper = (StateWrapper) state;
            Console.WriteLine($"Job [{stateWrapper.JobIndex}] started...");
            using var customerStorageManager = new CustomerStorageManager(_connectionString);
            customerStorageManager.CreateCustomersRange(stateWrapper.Customers);
            stateWrapper.WaitHandle?.Set();
            Console.WriteLine($"Job [{stateWrapper.JobIndex}] finished.");
        }

        internal struct StateWrapper
        {
            public AutoResetEvent WaitHandle { get; set; }
            public List<Customer> Customers { get; set; }
            public int JobIndex { get; set; }
        }
    }
}