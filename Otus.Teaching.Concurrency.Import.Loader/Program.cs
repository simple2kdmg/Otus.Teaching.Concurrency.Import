﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.DataAccess.Serializers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;


namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.csv");
        private static string _generatorDirectory =
            @"D:\Workspace\Projects\Otus.Teaching.Concurrency.Import\Otus.Teaching.Concurrency.Import.Loader\bin\Debug\netcoreapp3.1";
        private static string _generatorName = "Otus.Teaching.Concurrency.Import.Generator.exe";

        private static string _connectionString =
            "Host=localhost;port=5432;Database=postgres;Username=otus;Password=otus;";


        static async Task Main(string[] args)
        {
            if (args != null && args.Length == 1)
            {
                _dataFilePath = args[0];
            }

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");
            Console.WriteLine("Type [y] to generate file in separate process; [n] or nothing to generate as method.");
            var answer = Console.ReadKey();
            Process process = null;
            Console.WriteLine(Environment.NewLine);

            if (answer.KeyChar == 'y' || answer.KeyChar == 'Y') process = GenerateInSeparateProcess();
            else await GenerateUsingMethod();
            Console.WriteLine(Environment.NewLine);

            LoadData(process);

            Console.ReadKey();
        }

        static Process GenerateInSeparateProcess()
        {
            Console.WriteLine("Generating in separate process." + Environment.NewLine);
            var startInfo = new ProcessStartInfo()
            {
                ArgumentList = {_dataFilePath.ToString(), "1000000"},
                FileName = Path.Combine(_generatorDirectory, _generatorName)
            };

            return Process.Start(startInfo);
        }

        static async Task GenerateUsingMethod()
        {
            Console.WriteLine("Generating using method." + Environment.NewLine);
            var customersFileGenerator = new CustomersFileGenerator(_dataFilePath, 1000000);
            await customersFileGenerator.GenerateAsync();
        }

        static void LoadData(Process process)
        {
            process?.WaitForExit(30000);
            var loader = new ParallelDataLoader(new Serializer<Customer>(), _connectionString, 10);
            loader.LoadData(_dataFilePath);
        }
    }
}