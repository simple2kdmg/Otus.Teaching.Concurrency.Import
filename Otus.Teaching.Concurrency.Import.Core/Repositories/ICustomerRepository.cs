﻿using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Core.Repositories
{
    public interface ICustomerRepository
    {
        public IReadOnlyList<Customer> Get();
        public Customer Get(int customerId);
        public int CreateCustomer(Customer entity);
        public int CreateCustomersRange(IList<Customer> entities);
    }
}