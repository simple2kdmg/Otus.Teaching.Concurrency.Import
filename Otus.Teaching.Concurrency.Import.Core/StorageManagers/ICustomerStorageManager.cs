﻿using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Core.StorageManagers
{
    public interface ICustomerStorageManager
    {
        public IReadOnlyList<Customer> GetCustomers();
        public Customer GetCustomer(int customerId);
        public int CreateCustomer(Customer customer);
        public int CreateCustomersRange(IList<Customer> customers);
    }
}