﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.Concurrency.Import.Core.Serializers
{
    public interface ISerializer<T>
    {
        string Serialize(T target);
        T Deserialize(string target);
        string SerializeList(IList<T> target);
        List<T> DeserializeList(string target);
    }

}