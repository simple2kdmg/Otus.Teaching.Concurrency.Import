﻿using System;

namespace Otus.Teaching.Concurrency.Import.Core.Extensions
{
    public static class ParserExtension
    {
        public static object ParseValue(this object value, Type propertyType)
        {
            if (value == null)
            {
                return null;
            }

            var type = Nullable.GetUnderlyingType(propertyType) ?? propertyType;

            return Convert.ChangeType(value, type);
        }
    }
}