﻿using System;
using System.IO;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core.Serializers;
using Otus.Teaching.Concurrency.Import.DataAccess.Serializers;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class CustomersFileGenerator : IDataGenerator
    {
        private readonly string _fileName;
        private readonly int _dataCount;
        private readonly ISerializer<Customer> _serializer;

        public CustomersFileGenerator(string fileName, int dataCount)
        {
            _fileName = fileName;
            _dataCount = dataCount;
            _serializer = new Serializer<Customer>();
        }

        public async Task GenerateAsync()
        {
            Console.WriteLine("Generation start...");
            var customers = RandomCustomerGenerator.Generate(_dataCount);
            await File.WriteAllTextAsync(_fileName, _serializer.SerializeList(customers));
            Console.WriteLine("File generation finished...");
        }
    }
}