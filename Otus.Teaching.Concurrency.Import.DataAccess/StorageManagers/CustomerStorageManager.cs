﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using Npgsql;
using Otus.Teaching.Concurrency.Import.Core.Repositories;
using Otus.Teaching.Concurrency.Import.Core.StorageManagers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.StorageManagers
{
    public class CustomerStorageManager : ICustomerStorageManager, IDisposable
    {
        private IDbConnection _connection;
        private IDbTransaction _transaction;
        private ICustomerRepository _customerRepository;

        public CustomerStorageManager(string connectionString)
        {
            _connection = new NpgsqlConnection(connectionString);
            _connection.Open();
            _transaction = _connection.BeginTransaction();
        }

        public ICustomerRepository CustomersRepository =>
            _customerRepository ??= new CustomerRepository(_transaction);

        public IReadOnlyList<Customer> GetCustomers() =>
            CustomersRepository.Get();

        public Customer GetCustomer(int customerId) =>
            CustomersRepository.Get(customerId);

        public int CreateCustomer(Customer customer)
        {
            var result = CustomersRepository.CreateCustomer(customer);
            Commit();
            return result;
        }

        public int CreateCustomersRange(IList<Customer> customers)
        {
            Console.WriteLine($"CustomersRange created in Thread [{Thread.CurrentThread.ManagedThreadId}]");
            var result = CustomersRepository.CreateCustomersRange(customers);
            Commit();
            return result;
        }

        public void Commit()
        {
            try
            {
                _transaction.Commit();
            }
            catch
            {
                _transaction.Rollback();
                throw;
            }
            finally
            {
                _transaction.Dispose();
                _transaction = _connection.BeginTransaction();
                _customerRepository = null;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                ReleaseUnmanagedResources();
            }
        }

        private void ReleaseUnmanagedResources()
        {
            _transaction?.Dispose();
            _transaction = null;
            _connection?.Dispose();
            _connection = null;
        }

        ~CustomerStorageManager()
        {
            Dispose(false);
        }
    }
}