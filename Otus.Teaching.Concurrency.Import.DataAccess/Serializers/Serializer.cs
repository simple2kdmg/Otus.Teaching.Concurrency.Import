﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Otus.Teaching.Concurrency.Import.Core.Extensions;
using Otus.Teaching.Concurrency.Import.Core.Serializers;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Serializers
{
    public class Serializer<T> : ISerializer<T> where T : new()
    {
        private const char PropertySeparator = ',';
        private const char ValueSeparator = '=';

        public string Serialize(T target)
        {
            if (target == null) throw new ArgumentNullException(nameof(target));
            
            PropertyInfo[] infos = typeof(T).GetProperties();

            return String.Join(PropertySeparator, infos.Select(info => SerializeProperty(info, target)));
        }

        public string SerializeList(IList<T> target)
        {
            if (target == null || target.Count == 0) throw new ArgumentNullException(nameof(target));

            return String.Join(Environment.NewLine, target.Select(Serialize));
        }

        public T Deserialize(string target)
        {
            if (String.IsNullOrEmpty(target)) throw new ArgumentNullException(nameof(target));

            var deserializedProperties = GetDeserializedProperties(target);
            T result = new T();

            foreach (var info in typeof(T).GetProperties())
            {
                var propertyValue = deserializedProperties.GetValueOrDefault(info.Name, null);
                info.SetValue(result, propertyValue.ParseValue(info.PropertyType));
            }

            return result;
        }

        public List<T> DeserializeList(string target)
        {
            if (String.IsNullOrEmpty(target) || String.IsNullOrWhiteSpace(target)) throw new ArgumentNullException(nameof(target));

            return target.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries)
                .Select(Deserialize)
                .ToList();
        }

        private string SerializeProperty(PropertyInfo info, T target)
        {
            return $"{info.Name}{ValueSeparator}{info.GetValue(target)}";
        }

        private Dictionary<string, string> GetDeserializedProperties(string target)
        {
            var result = new Dictionary<string, string>();

            foreach (var deserializedProperty in target.Split(PropertySeparator))
            {
                if (String.IsNullOrEmpty(deserializedProperty)) continue;

                var splitted = deserializedProperty.Split(ValueSeparator);
                result.Add(splitted[0], splitted[1]);
            }

            return result;
        }
    }

}