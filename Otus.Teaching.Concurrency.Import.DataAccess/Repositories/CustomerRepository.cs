﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Otus.Teaching.Concurrency.Import.Core.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository : BaseDatabaseRepository, ICustomerRepository
    {
        public CustomerRepository(IDbTransaction transaction) : base(transaction)
        {

        }

        public IReadOnlyList<Customer> Get()
        {
            var sql = @"SELECT * FROM customers";
            var result = Connection.Query<Customer>(sql);
            return result.ToImmutableList();
        }

        public Customer Get(int customerId)
        {
            var sql = @"SELECT * FROM customers WHERE customers.id = @customerId";
            var result = Connection.Query<Customer>(sql, new { customerId });
            return result.SingleOrDefault();
        }

        public int CreateCustomer(Customer entity)
        {
            if (entity == null) return 0;

            var sql = @"
                INSERT INTO customers (id, fullname, email, phone)
                VALUES (@Id, @FullName, @Email, @Phone)
            ";
            return Connection.Execute(sql, entity, Transaction);
        }

        public int CreateCustomersRange(IList<Customer> customers)
        {
            var sql = @"
                INSERT INTO customers (id, fullname, email, phone)
                VALUES (@Id, @FullName, @Email, @Phone)
            ";
            return Connection.Execute(sql, customers, Transaction);
        }
    }
}