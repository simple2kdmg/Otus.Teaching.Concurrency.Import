using System.Data;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public abstract class BaseDatabaseRepository
    {
        protected IDbTransaction Transaction { get; }
        protected IDbConnection Connection => Transaction?.Connection;

        protected BaseDatabaseRepository(IDbTransaction transaction)
        {
            Transaction = transaction;
        }
    }
}