﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Text.Json;

namespace Otus.Teaching.Concurrency.Import.ConsoleClient
{
    public class CustomersService : IDisposable
    {
        private readonly string _apiUrl;
        private readonly HttpClient _httpClient;

        public CustomersService(string apiUrl)
        {
            _apiUrl = apiUrl;
            _httpClient = new HttpClient();
        }

        public async Task<Customer> GetCustomerById(int customerId)
        {
            var response = await _httpClient.GetAsync($"{_apiUrl}/customers/get?id={customerId}");
            if (response.IsSuccessStatusCode)
            {
                var jsonContent = await response.Content.ReadAsStringAsync();
                var options = new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
                return JsonSerializer.Deserialize<Customer>(jsonContent, options);
            }

            return null;
        }

        public async Task<string> CreateRandomCustomer(Customer entity)
        {
            var response = await _httpClient.PostAsJsonAsync($"{_apiUrl}/customers/create", entity);
            if (response.IsSuccessStatusCode)
            {
                return $"Customer with id = {entity.Id} successfully created.";
            }

            return $"HttpResponse error. Status code: {response.StatusCode}. Message: {response.ReasonPhrase}";
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                ReleaseUnmanagedResources();
            }
        }

        private void ReleaseUnmanagedResources()
        {
            _httpClient.Dispose();
        }

        ~CustomersService()
        {
            Dispose(false);
        }
    }
}