﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;

namespace Otus.Teaching.Concurrency.Import.ConsoleClient
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var configuration = AddConfigurationFile();
            await Init(configuration);
        }

        public static IConfiguration AddConfigurationFile()
        {
            return new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false, true)
                .Build();
        }

        public static async Task Init(IConfiguration configuration)
        {
            using var customerService = new CustomersService(configuration.GetValue<string>("apiUrl"));
            var pattern = "^(\\w*)\\((\\d*)\\)$";
            Console.WriteLine("Available commands: GetCustomer(customerId), CreateRandomCustomer(), Exit()" + Environment.NewLine);

            while (true)
            {
                var nextCommand = Console.ReadLine();

                if (String.IsNullOrEmpty(nextCommand))
                {
                    Console.WriteLine("Command should not be empty.");
                    continue;
                }

                var parsed = Regex.Match(nextCommand, pattern);
                var commandName = parsed.Groups[1].Value.Trim();
                var customerId = parsed.Groups[2].Value;

                switch (commandName)
                {
                    case ("GetCustomer"):
                        var customer = await customerService.GetCustomerById(int.Parse(customerId));
                        Console.WriteLine(customer == null ? "Customer not found" : customer + Environment.NewLine);
                        break;
                    case ("CreateRandomCustomer"):
                        var randomCustomer = RandomCustomerGenerator.Generate(1).Single();
                        randomCustomer.Id = new Random().Next(1, 10);
                        var responseMessage = await customerService.CreateRandomCustomer(randomCustomer);
                        Console.WriteLine(responseMessage + Environment.NewLine);
                        break;
                    case ("Exit"):
                        return;
                }
            }
        }
    }
}
